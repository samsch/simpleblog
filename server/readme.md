# Simpleblog

Proof of concept for server.js

## To Do/Research List

- Use Knex 0.16.4's working per-builder event listening to setup pre-request query logging.
- Define how server.js configuration will work.
- Decide how to deal with "required" routes (logout, 404, error handling).
- Decide how to deal with rendering "widget" components which have data deps.
  - Data HoCs?
- Is Layout.js going to be part of the CMS or end user code?
