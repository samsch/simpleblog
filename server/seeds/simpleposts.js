'use strict';
const genId = require('uuid/v4');
const scrypt = require('scrypt-for-humans');

function makePost (userId, body, title, pathname) {
  return {
    id: genId(),
    body,
    title,
    pathname,
    user_id: userId,
  };
}

exports.seed = async knex => {
  const hashedPassword = await scrypt.hash('a password');
  const user = {
    id: genId(),
    name: 'Samuel Scheiderich',
    email: 'sam@samsch.org',
    password: hashedPassword,
  };
  // Deletes ALL existing entries
  await knex('post').del();
  await knex('user').del();
  await knex('user').insert(user);
  await knex('post').insert([
    makePost(user.id, 'This is a post body', 'Hi Post', '/hi-post'),
    makePost(user.id, 'This is another post body', 'Lo Post', '/lo-post'),
    makePost(user.id, 'This might be a post body?', 'Questionable', '/questionable'),
    makePost(user.id, 'Everybody is dancing', 'Dancing', '/dancing'),
  ]);
  return undefined;
};
