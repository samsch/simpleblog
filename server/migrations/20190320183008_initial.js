'use strict';
const Promise = require('bluebird');
exports.up = async knex => {
  const existing = (await Promise.map(
    ['user', 'note'],
    tableName => knex.schema.hasTable(tableName).then(exists => ({ tableName, exists }))
  ))
    .filter(({ exists }) => exists)
    .map(({ tableName }) => tableName);
  if (existing.length) {
    throw new Error(`These tables already exists: "${existing.join('", "')}"`);
  }
  await knex.schema.createTable('user', table => {
    table.uuid('id').primary();
    table.text('name').notNullable();
    table.text('email').notNullable();
    table.text('password');
    table.timestamps(true, true);
  });

  await knex.schema.createTable('post', table => {
    table.uuid('id').primary();
    table.text('body').notNullable();
    table.text('title').notNullable();
    table.text('pathname').notNullable();
    table
      .uuid('user_id')
      .references('id')
      .inTable('user')
      .notNullable();
    table.timestamps(true, true);
  });
};

exports.down = async knex => {
  await knex.schema.dropTableIfExists('post');
  await knex.schema.dropTableIfExists('user');
  return undefined;
};
