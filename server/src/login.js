'use strict';
const scrypt = require('scrypt-for-humans');
const randomNumber = require('random-number-csprng');
const Promise = require('bluebird');
const genId = require('uuid/v4');
const respond = require('./respond');
const { setConfirmCode, checkConfirmCode } = require('./confirmCode');

async function sendLoginCodeEmail (mailer, email, code) {
  return mailer.sendEmail({
    to: email,
    from: 'noreply@samsch.org',
    subject: 'RealNote login code',
    text: `Hi! This is your login code for RealNote: ${code}

You used email address: ${email}

If you didn't attempt to login or create a new RealNote file, you can safely ignore this email.`,
    html: `<p>Hi! This is your login code for RealNote: <b>${code}</b></p>
<p>You used email address: ${email}</p>
<p style="font-size: .9em;">If you didn't attempt to login or create a new RealNote file, you can safely ignore this email.</p>`,
  });
}

module.exports = ({ router, knex, mailer, addDebugData }) => {
  router.post('/login_password', (req, res) => {
    if (!req.body.email || !req.body.password) {
      addDebugData({ req, label: 'missing email or password', data: req.body });
      const formErrors = {};
      if (!req.body.email) {
        formErrors.email = 'Required';
      }
      if (!req.body.password) {
        formErrors.password = 'Required';
      }
      res.status(400);
      respond(res, 'error', {
        error: 'Email and Password are required.',
        formErrors,
      });
      return;
    }
    return Promise.try(() => {
      return randomNumber(0, 200);
    })
      .then(random => {
        return Promise.all([
          Promise.delay(random),
          knex('user').where({ email: req.body.email }),
        ]);
      })
      .then(async ([, users]) => {
        if (users.length !== 1) {
          addDebugData({ req, label: 'email/user not found', data: req.body.email });
          respond(res, 'error', {
            message: 'Email and Password combination is incorrect. Check what you entered and try again.',
          });
          return;
        }
        const user = users[0];
        const isPasswordCorrect = scrypt
          .verifyHash(req.body.password, user.password)
          .then(() => true)
          .catch(scrypt.PasswordError, () => false);
        if (!isPasswordCorrect) {
          respond(res, 'error', {
            message: 'Email and Password combination is incorrect. Check what you entered and try again.',
          });
          return;
        }
        addDebugData({ req, label: 'email/password valid' });
        await Promise.fromCallback(callback =>
          req.session.regenerate(callback)
        )
          .catch(error => {
            addDebugData({ req, label: 'failed to regerated session', data: error.message });
            res.set('csrf-token', req.csrfToken());
            console.log('Failed to regenerate session for login', error);
            throw error;
          });
        addDebugData({ req, label: 'user logged in', data: req.body.email });
        req.session.user_id = user.id;
        res.set('csrf-token', req.csrfToken());
        respond(res, 'success', {
          message: 'Successfully logged in',
          user: {
            id: user.id,
            name: user.name,
            email: user.email,
          },
        });
      })
      .catch(error => {
        addDebugData({ req, label: 'login crashed', data: error.message });
        console.log('Login failed', error);
        throw error;
      });
  });

  router.post('/login_email_code', async (req, res) => {
    if (!req.body.email) {
      addDebugData({ req, label: 'missing email', data: req.body });
      res.status(400);
      respond(res, 'error', {
        error: 'Email and Password are required.',
        fieldErrors: {
          email: 'Required',
        },
      });
    }
    const email = req.body.email;
    const code = await randomNumber(100000, 999999);
    await sendLoginCodeEmail(mailer, email, code);
    setConfirmCode(req, code, email);
    addDebugData(() => [req, 'sending email confirmation', { code }]);
    respond(res, 'success');
  });

  // function exists (props) {
  //   const obj = {};
  //   Object.entries(props).forEach(([key, val]) => {
  //     if (val) {
  //       obj[key] = 'required';
  //     }
  //   });
  // }

  router.post('/login_email_confirm', async (req, res) => {
    const { code, email } = req.body;
    if (!code || !email) {
      respond(res, 'error', {
        message: 'Whoops, something broke. Please try refreshing the page and re-logging in.',
        fieldErrors: {
          code: !code ? 'Code is required' : undefined,
          email: !email ? 'Email is required' : undefined,
        },
      });
      return;
    }
    if (!checkConfirmCode(req, code, email)) {
      respond(res, 'error', { type: 'wrong code' });
      return;
    }
    const [user, newUser] = await knex('user')
      .select('id', 'name', 'email')
      .where({ email })
      .then(async userResult => {
        if (userResult[0]) {
          return [userResult[0], false];
        }
        const id = genId();
        const user = {
          id,
          email,
          name: 'Anonymous User',
        };
        await knex('user').insert(user);
        return [user, true];
      });

    await Promise.fromCallback(callback =>
      req.session.regenerate(callback)
    )
      .catch(error => {
        addDebugData({ req, label: 'failed to regerated session', data: error.message });
        res.set('csrf-token', req.csrfToken());
        console.log('Failed to regenerate session for login', error);
        throw error;
      });
    addDebugData({ req, label: 'user logged in', data: email });
    req.session.user_id = user.id;
    res.set('csrf-token', req.csrfToken());
    respond(res, 'success', {
      message: 'Successfully logged in',
      newUser,
      user,
    });
  });

  return router;
};
