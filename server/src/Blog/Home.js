'use strict';
const React = require('react');
// const styles = require('./home.scss');

const thisYear = new Date().getFullYear();

const Home = (req, blog) => {
  return (
    <>
      <h2>Blogging since {thisYear}</h2>
      <h3>and maybe earlier...</h3>
      <p>Welcome to {blog.title}!</p>
    </>
  );
};
module.exports = Home;
