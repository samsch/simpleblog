'use strict';

const blog = {
  title: 'My Blog',
  subTitle: null,
  rootPath: '/',
  pages: [
    {
      primary: true,
      path: '/',
      label: 'Home',
      renderPage: require('./Home'),
    },
    {
      primary: true,
      path: '/posts',
      label: 'All Posts',
      renderPage: require('./Posts'),
    },
    {
      primary: true,
      label: '404',
      path: '/not-found',
    },
  ],
};

module.exports = blog;
