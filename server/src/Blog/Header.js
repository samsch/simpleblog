'use strict';
const React = require('react');
const PropTypes = require('prop-types');
const styles = require('./header.scss');
const conditionalMap = require('./util/conditionalMap');

const Header = ({ blog, req }) => {
  return (
    <div className={styles.header}>
      <h1
        className={styles.title}
      >
        <a href={blog.rootPath}>{blog.title}</a>
      </h1>
      {blog.subTitle && <h2>{blog.subTitle}</h2>}
      <nav className={styles.nav}>
        {conditionalMap(blog.pages, (page, none) => {
          if (page.primary) {
            return (
              <a
                href={page.path}
                className={page.path === req.originalUrl ? `${styles.navLink} ${styles.active}` : styles.navLink}
              >
                {page.label || page.path}
              </a>
            );
          }
          return none;
        })}
      </nav>
    </div>
  );
};
Header.propTypes = {
  blog: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
    rootPath: PropTypes.string.isRequired,
    pages: PropTypes.arrayOf(PropTypes.shape({
      label: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
    })).isRequired,
  }).isRequired,
  req: PropTypes.shape({
    originalUrl: PropTypes.string.isRequired,
  }).isRequired,
};
module.exports = Header;
