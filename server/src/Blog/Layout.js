'use strict';
const React = require('react');
const PropTypes = require('prop-types');
const styles = require('./layout.scss');
const { Provider } = require('./util/blogContext');
const Header = require('./Header');
const Footer = require('./Footer');

const Layout = ({ children, ...props }) => {
  return (
    <Provider value={props}>
      <Header {...props} />
        <div className={styles.content}>
          {children}
        </div>
      <Footer {...props} />
    </Provider>
  );
};
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
module.exports = Layout;
