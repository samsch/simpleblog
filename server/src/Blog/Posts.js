'use strict';
const React = require('react');
const styles = require('./posts.scss');

const Promise = require('bluebird');
module.exports = async (req) => {
  const posts = await getPosts(req.user, req.knex);

  return (
    <div className={styles.posts}>
      <h2>Posts</h2>
      <div>
        {posts.map(post => {
          return (
            <div className={styles.post}>
              <h3>{post.title}</h3>
              <h4>
                <a href={`/post/${post.slug}`}></a>
              </h4>
              <pre>
                {post.body}
              </pre>
            </div>
          );
        })}
      </div>
    </div>
  );
};

async function getPosts (/*user, knex*/) {
  await Promise.delay(500);
  return [
    {
      title: 'Last post ever made',
      body: `
How silly is this?

I write, therefore, I am.

Pendulum.
`,
      slug: 'last-post-ever-made',
      date: '2019-04-11',
    },
    {
      title: 'First post ever made',
      body: `
This is so serious.

Can't think, eat, or sleep. Wat.

Increased Penelope.
`,
      slug: 'first-post-ever-made',
      date: '2019-04-10',
    },
  ];
}
