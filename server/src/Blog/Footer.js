'use strict';
const React = require('react');
const styles = require('./footer.scss');
const conditionalMap = require('./util/conditionalMap');

const thisYear = new Date().getFullYear();

const Footer = ({ blog }) => {
  return (
    <div className={styles.footer}>
      <nav className={styles.footerPrimaryNav}>
        {conditionalMap(blog.pages, (page, none) => {
          if (page.primary) {
            return (
              <a href={page.path}>{page.label || page.path}</a>
            );
          }
          return none;
        })}
      </nav>
      <div>
        Copyright {thisYear}, Samuel Scheiderich. The source code of this application is licensed under 0BSD, which means you may use it for any purpose, without attribution.
      </div>
    </div>
  );
};
module.exports = Footer;
