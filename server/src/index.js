'use strict';
const server = require('./server');
const debug = process.env.NODE_ENV === 'development';
const config = require('../../config');
const Mailer = require('./mailer');
// const userRoutes = require('./user');
// const loginRoute = require('./login');
const { static: staticMiddleware } = require('express');
const makeRouter = require('express-promise-router');
const React = require('react');
const mailer = Mailer();

const blog = require('./Blog/blog');
const Layout = require('./Blog/Layout');

const blogRoutes = function (appFeatures) {
  const { router, addDebugData, render } = appFeatures;
  blog.pages.forEach(page => {
    const {
      path,
      label,
      renderPage,
    } = page;
    if (renderPage) {
      router.get(path, async (req, res) => {
        const rendered = await renderPage(req, blog, appFeatures);
        render({
          req,
          res,
          title: 'Hello, world!',
          element: (
            <Layout {...{ req, blog }}>
              {rendered}
            </Layout>
          ),
        });
        addDebugData(() => [req, 'Rendering', { path, label }]);
      });
    }
  });
  return router;
};

const notFoundRoutes = function ({ router, addDebugData, render }) {
  router.use((req, res, next) => {
    if (!req.accepts('html')) {
      next();
      return;
    }
    const appProps = {
      req,
      blog,
    };
    render({
      req,
      res,
      title: 'Page not found',
      element: (
        <Layout {...appProps}>
          <p>Sorry, the request page doesn&apos;t seem to exist. :(</p>
        </Layout>
      ),
    });
    addDebugData(() => [req, 'Rendered 404 not found page', {}]);
  });
  return router;
};

function getRoutes ({ addDebugData }) {
  const render = server.makeReactRenderer({
    addDebugData,
    defaultStylesheets: ['/css/styles.css'],
  });
  return [
    blogRoutes,
    // userRoutes,
    // loginRoute,
    ({ router }) => { router.use('/css', staticMiddleware(__dirname + '/../css')); return router; },
    server.makeStaticRoutes(__dirname + '/../../static/'),
    notFoundRoutes,
  ].map(route => route({ router: makeRouter(), addDebugData, mailer, render }));
}

server({
  getRoutes,
  config: {
    ...config,
  },
  debug,
});
