'use strict';
const Promise = require('bluebird');
const respond = require('./respond');

function userRoutes ({ router, knex, addDebugData }) {
  router.use(async (req, res, next) => {
    if (req.session.user_id) {
      const [user] = await knex('user')
        .select('id', 'name', 'email', knex.raw('"password" IS NOT NULL as "password"'))
        .where({ id: req.session.user_id });
      if (!user) {
        await Promise.fromCallback(cb => req.session.destroy(cb));
        addDebugData({ req, label: 'Session user not found!', data: req.session.user_id });
      } else {
        addDebugData({ req, label: 'Session user loaded', data: user});
        req.user = { id: user.id, name: user.name, email: user.email, password: user.password };
        req.hasRole = (role) => {
          if (role === 'user') {
            return true;
          }
        };
      }
    } else {
      req.hasRole = () => false;
    }
    next();
  });

  router.get('/user', async (req, res) => {
    if (req.user) {
      respond(res, 'success', {
        user: {
          id: req.user.id,
          name: req.user.name,
          email: req.user.email,
        },
      });
    } else {
      res.status(404);
      respond(res, 'success', { user: null });
    }
  });

  return router;
}

module.exports = userRoutes;
